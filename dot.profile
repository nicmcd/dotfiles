#!/bin/sh

# source ~/.bashrc if running interactively AND it exists
if [[ $- == *i* ]]; then
    if [ -f ~/.bashrc ] || [ -h ~/.bashrc ]; then
	source ~/.bashrc
    else
	echo "No ~/.bashrc found! Lame environment ensues."
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi
