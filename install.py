#!/usr/bin/env python3

import argparse
import glob
import os
import subprocess

def gitconfig(dst):
  """
  This sets the github information in the gitconfig file
  """
  if dst.endswith('gitconfig'):
    name = input('gitconfig: what is your name? ')
    assert name, 'Empty name'
    email = input('gitconfig: what is your email? ')
    assert email, 'Empty email'
    username = input('gitconfig: what is your username? ')
    assert username, 'Empty username'
    return [(dst, '<<NAME>>', name),
            (dst, '<<EMAIL>>', email),
            (dst, '<<USERNAME>>', username)]
  else:
    return []


filters = []
filters.append(gitconfig)


def main(args):
  # create a list of post installation changes to be made
  changes = []

  # create a map between source and destinations
  files = {}
  base = os.path.expanduser('~/')
  for src in glob.glob('dot.*'):
    dst = os.path.join(base, src[3:])
    files[src] = dst
    # allow filters to intercept content
    for filter_func in filters:
      changes.extend(filter_func(dst))

  # install new files
  for src in files.keys():
    dst = files[src]
    print('{0} -> {1}'.format(src, dst))
    install = 'cp {0} {1} {2}'.format(
      '-i' if not args.force else '',
      src, dst)
    if not args.debug:
      subprocess.check_call(install, shell=True)
    else:
      print('  WOULD: {}'.format(install))
    for change in changes:
      if change[0] == dst:
        assert change[1].find('/') < 0
        assert change[2].find('/') < 0
        replace = 'sed -i \'s/{}/{}/g\' {}'.format(
            change[1], change[2], change[0])
        if not args.debug:
          subprocess.check_call(replace, shell=True)
        else:
          print('  WOULD: {}'.format(replace))

  return 0


if __name__ == '__main__':
  ap = argparse.ArgumentParser()
  ap.add_argument('-f', '--force', action='store_true',
                  help='force file overwrite')
  ap.add_argument('-d', '--debug', action='store_true',
                  help='don\'t perform actions')
  args = ap.parse_args()
  main(args)
